# File: /Makefile
# Project: mkpm
# File Created: 26-09-2021 00:47:48
# Author: Clay Risser
# -----
# Last Modified: 06-10-2021 16:10:15
# Modified By: Clay Risser
# -----
# BitSpur Inc (c) Copyright 2021
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include mkpm.mk
ifneq (,$(MKPM))
-include $(MKPM)/mkchain
-include $(MKPM)/mkgnu

TMP_DIR ?= $(MKPM_TMP)/portable-git
CURL ?= curl
ZIP ?= zip
WINE ?= wine
PORTABLE_GIT_VERSION ?= 2.33.0.2
PORTABLE_GIT_SEMVER := $(shell echo $(PORTABLE_GIT_VERSION) | $(SED) 's|\.[^\.]\+$$||g')

export WIN64_DOWNLOAD := \
	https://github.com/git-for-windows/git/releases/download/v$(PORTABLE_GIT_SEMVER).windows.2/PortableGit-$(PORTABLE_GIT_VERSION)-64-bit.7z.exe
export WIN32_DOWNLOAD := \
	https://github.com/git-for-windows/git/releases/download/v$(PORTABLE_GIT_SEMVER).windows.2/PortableGit-$(PORTABLE_GIT_VERSION)-32-bit.7z.exe

.DEFAULT_GOAL := pack

$(TMP_DIR)/Win%/PortableGit.exe:
	@$(call mkdir_p,$(@D))
	@$(CURL) -L -o $@ $(WIN$*_DOWNLOAD)

$(TMP_DIR)/Win%/PortableGit/git-cmd.exe: $(TMP_DIR)/Win%/PortableGit.exe
	@$(WINE) $<

$(TMP_DIR)/Win%PortableGit.zip: $(TMP_DIR)/Win%/PortableGit/git-cmd.exe
	@$(CD) $(TMP_DIR)/Win$*/PortableGit && $(ZIP) -r $@ .

release/Win%PortableGit.zip: $(TMP_DIR)/Win%PortableGit.zip
	@$(call mkdir_p,$(@D))
	@$(CP) $< $@

ACTIONS += pack
$(ACTION)/pack: release/Win32PortableGit.zip \
	release/Win64PortableGit.zip

.PHONY: clean
clean:
	@$(GIT) clean -fXd

.PHONY: purge
purge: clean

-include $(call actions)

endif
